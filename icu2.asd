#|
  This file is a part of icu2 project.
  Copyright (c) 2016 Matt Noventstern (fisxoj@gmail.com)
|#

#|
  Author: Matt Noventstern (fisxoj@gmail.com)
|#

(in-package :cl-user)
(defpackage icu2-asd
  (:use :cl :asdf))
(in-package :icu2-asd)

(defsystem icu2
  :version "0.1"
  :author "Matt Noventstern"
  :license ""
  :depends-on (:mgl
               :mgl-mat
               :alexandria
               :iterate
               :trivial-download
               :flexi-streams
               :function-cache)
  :components ((:module "src"
                :components
                ((:file "icu2")
                 (:file "loader")
                 (:file "trainer"))))
  :description ""
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.org"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op icu2-test))))
