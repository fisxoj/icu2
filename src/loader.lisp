(defpackage icu2.loader
  (:use :cl :alexandria :iterate :trivial-download)
  (:export #:load-data
           #:make-nist-sampler))

(in-package #:icu2.loader)

(defparameter +data-url+ "http://yann.lecun.com/exdb/mnist/"
  "Location of the MINST data set files.")

(defparameter +local-data-dir+ (merge-pathnames "data/")
  "Local location of the MINST data.")

(defparameter +image-height+ 28
  "Height of the MINST image data.")

(defparameter +image-width+ 28
  "Width of the MINST image data.")

(defmacro ensuring-and-gunzipping-file ((stream filename) &body body)
  (alexandria:with-gensyms (input)
    `(labels ((retry ()
                (handler-case
                    (with-open-file (,input download-path :element-type 'flexi-streams:octet)

                      ;; Decompress the data!
                      (flexi-streams:with-input-from-sequence (,stream (chipz:decompress nil 'chipz:gzip ,input))

                        ,@body))

                  (file-error (e)
                    (declare (ignore e))

                    ;; The file isn't already downloaded.  Get it and try again.
                    (download (concatenate 'string +data-url+ (file-namestring ,filename)) download-path)
                    (retry)))))
       (retry))))

(function-cache:defcached load-data (filename)
  (let ((download-path (merge-pathnames filename +local-data-dir+)))
    (ensuring-and-gunzipping-file (input download-path)
      ;; Massage the data into mgl-friendly formats

      ;; Skip 4 bytes
      (read-low-endian-ub32 input)
      (let ((number-of-images (read-low-endian-ub32 input))
            (rows             (read-low-endian-ub32 input))
            (columns          (read-low-endian-ub32 input)))
        (assert (= rows    +image-width+))
        (assert (= columns +image-height+))

        (format t "~&Loading ~d images~%" number-of-images)

        (iter (for i from 0 below number-of-images)
              ;; Resuable storage for the data
              (with temp-storage = (make-array (* +image-width+ +image-height+)

                                               :element-type '(unsigned-byte 8)))

                                        ;            (when (zerop (mod i 1000)) (print i))

              (read-sequence temp-storage input)
              (collect (read-sequence-to-matrix temp-storage)))))))

(defun read-low-endian-ub32 (stream)
  (+ (ash (read-byte stream) 24)
     (ash (read-byte stream) 16)
     (ash (read-byte stream) 8)
     (read-byte stream)))

(function-cache:defcached load-labels (filename)
  (let ((download-path (merge-pathnames filename +local-data-dir+)))
    (ensuring-and-gunzipping-file (input download-path)

      (assert (= (read-low-endian-ub32 input) 2049))

      (let ((n (read-low-endian-ub32 input)))
        (format t "~&Reading ~d labels~%" n)
        (iter (repeat n)
              (collect (read-byte input)))))))

(defun read-sequence-to-matrix (sequence &key (max 1d0) (min 0d0))
  "Map a byte array onto a 1D float array with a range from min to max."

  (let ((output (make-array (* +image-width+ +image-height+) :element-type 'double-float))
        (i-min 0)
        (i-max 255))

    (iter (for i from 0 below (array-total-size output))
          ;; For $min_{in} \leq x \leq max_{in}$ scaling to $min \leq y \leq max$ ,
          ;; $$ y = \frac{(x-min_{in})\cdot(max-min)}{max_{in}-min_{in}} + min $$
          (setf (row-major-aref output i)
                (+ (* (/ (- (row-major-aref sequence i) i-min)
                         (- i-max i-min))
                      (- max min))
                   min)))
    (mgl-mat:array-to-mat output)))

(defun make-nist-sampler (images-filename labels-filename)
  "Make a mgl sampler for the MNIST data that returns (matrix number-label) for each image/label pair."

  (let ((data (mapcar (lambda (image label)
                        (list image label))
                      (load-data   images-filename)
                      (load-labels labels-filename)))
        (current-image -1))

    (make-instance 'mgl:function-sampler
                   :max-n-samples (length data)
                   :generator (lambda () (nth (incf current-image) data)))))
