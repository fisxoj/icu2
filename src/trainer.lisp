(defpackage icu2.trainer
  (:use :cl :mgl)
  (:export #:train-mnist))

(in-package #:icu2.trainer)

(defparameter *last-nn* nil)

(defun train-mnist ()
  (let ((fnn (build-fnn (:name 'minst-fnn :max-n-stripes 5)
               (input (->input :size 784))
               (hidden-activation (->activation input :size 15))
               (hidden (->sigmoid hidden-activation))
               (output-activation (->activation hidden :size 10))
               (output (->softmax-xe-loss output-activation))))

        ;; Use a segmented gradient descent optimizer
        (optimizer (make-instance 'mgl:segmented-gd-optimizer
                                  :segmenter (constantly
                                              (make-instance 'mgl:sgd-optimizer
                                                             :learning-rate 1
                                                             :momentum 0.9
                                                             :batch-size 100)))))

    ;; Store a reference to this net for convenience
    (setf *last-nn* fnn)

    ;; Initialize the weights with some random values
    (map-segments (lambda (weights)
                    (gaussian-random! (nodes weights) :stddev 0.01))
                  fnn)

    (monitor-optimization-periodically
     optimizer '((:fn log-test-error              :period 1000)
                 (:fn reset-optimization-monitors :period 1000)))

    ;; The optimization
    (minimize optimizer
              (make-instance 'bp-learner
                             :bpn fnn
                             :monitors (make-cost-monitors fnn :attributes '(:event "train")))
              :dataset (icu2.loader:make-nist-sampler
                        "train-images-idx3-ubyte.gz"
                        "train-labels-idx1-ubyte.gz"))))


;;; From https://github.com/melisgl/mgl/blob/master/example/digit-fnn.lisp
;;; Log the test error. Also, describe the optimizer and the bpn at
;;; the beginning of training. Called periodically during training
;;; (see above).
(defun log-test-error (optimizer learner)
  (when (zerop (n-instances optimizer))
    (describe optimizer)
    (describe (mgl:bpn learner)))
  (log-padded
   (monitor-bpn-results (icu2.loader:make-nist-sampler
                         "train-images-idx3-ubyte.gz"
                         "train-labels-idx1-ubyte.gz")
                        (bpn learner)
                        (make-cost-monitors
                         (bpn learner) :attributes `(:event "pred.")))))

(defmethod mgl:set-input (data-and-labels (fnn fnn))
  ;; Going by the method defined in https://github.com/melisgl/mgl/blob/master/example/digit-fnn.lisp,
  ;; this sets the input clump and also the expected output of the network.  Who knew!

  (let ((input-nodes (nodes  (find-clump 'input  fnn))))

    ;; Copy input data to input of fnn
    (mgl-mat:map-concat #'copy! data-and-labels input-nodes :key #'first)
    (setf (target (find-clump 'output fnn)) (mapcar #'second data-and-labels))))

;; (function-cache:defcached make-target-from-label (label)
;;   (case label
;;     (0 (array-to-mat #(0d0 0d0 0d0 0d0)))
;;     (1 (array-to-mat #(0d0 0d0 0d0 1d0)))
;;     (2 (array-to-mat #(0d0 0d0 1d0 0d0)))
;;     (3 (array-to-mat #(0d0 0d0 1d0 1d0)))
;;     (4 (array-to-mat #(0d0 1d0 0d0 0d0)))
;;     (5 (array-to-mat #(0d0 1d0 0d0 1d0)))
;;     (6 (array-to-mat #(0d0 1d0 1d0 0d0)))
;;     (7 (array-to-mat #(0d0 1d0 1d0 1d0)))
;;     (8 (array-to-mat #(1d0 0d0 0d0 0d0)))
;;     (9 (array-to-mat #(1d0 0d0 0d0 1d0)))
;;     (t (error "~a isn't a number 0 <= n < 10" label))))
