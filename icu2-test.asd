#|
  This file is a part of icu2 project.
  Copyright (c) 2016 Matt Noventstern (fisxoj@gmail.com)
|#

(in-package :cl-user)
(defpackage icu2-test-asd
  (:use :cl :asdf))
(in-package :icu2-test-asd)

(defsystem icu2-test
  :author "Matt Noventstern"
  :license ""
  :depends-on (:icu2
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "icu2"))))
  :description "Test system for icu2"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
